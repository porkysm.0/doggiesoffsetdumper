package main

import (
	"fmt"
	"github.com/hashicorp/hcl/v2/hclsimple"
	"github.com/urfave/cli/v2"
	"log"
	"os"
)

var configFile string
var binaryPath string

var app = &cli.App{
	Name:  "doggiesOffsetDumper",
	Usage: "dump doggies offsets",
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:        "config",
			Value:       "config.hcl",
			Usage:       "The config file to use",
			Destination: &configFile,
		},
		&cli.StringFlag{
			Name:        "file",
			Usage:       "The binary file to use",
			Destination: &binaryPath,
			Required:    true,
		},
	},
	Action: func(c *cli.Context) error {
		return execConfig(configFile, binaryPath)
	},
}

func main() {
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func execConfig(configFile, binaryPath string) error {
	var cfg Config

	if err := hclsimple.DecodeFile(configFile, nil, &cfg); err != nil {
		return err
	}

	// Process signature blocks
	for _, sig := range cfg.Signature {
		result, err := sig.Resolve(binaryPath)
		if err != nil {
			return err
		}

		fmt.Println(sig.FormatResult(result, binaryPath))
	}

	return nil
}
