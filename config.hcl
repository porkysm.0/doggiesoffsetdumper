signature "state" {
  pattern = "89 15 ? ? ? ? 74 43"
  offset = 2
  size = 4
  type = "ptr"
}

signature "level_end" {
  pattern = "A1 ? ? ? ? 85 C0 0F 84 ? ? ? ? 6A 01"
  offset = 1
  size = 4
  type = "ptr"
}

signature "level_id" {
  pattern = "A3 ? ? ? ? A1 ? ? ? ? 85 C0 75 11"
  offset = 1
  size = 4
  type = "ptr"
}

signature "level_ptr" {
  pattern = "A1 ? ? ? ? 53 55 56 85 C0"
  offset = 1
  size = 4
  type = "ptr"
}

signature "level_var_list" {
  pattern = "8B 43 ? 85 C0 74 0B 8B 0D ? ? ? ? 03 C1 89"
  offset = 2
  size = 1
  type = "literal"
}

signature "first_actor" {
  pattern = "C7 05 ? ? ? ? ? ? ? ? 8B 30"
  offset = 2
  size = 4
  type = "ptr"
}