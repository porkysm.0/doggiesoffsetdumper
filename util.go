package main

import "os"

func readBigEndian(f *os.File, offset, size int) (int, error) {
	data := make([]byte, size)

	if _, err := f.ReadAt(data, int64(offset)); err != nil {
		return 0, err
	}

	value := 0
	for i, v := range data {
		value += int(v) << (8 * i)
	}

	return value, nil
}
