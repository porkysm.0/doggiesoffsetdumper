# doggiesOffsetDumper

Tool to dump various memory offsets for PC versions of 102 Dalmatians: Puppies to the Rescue

## Usage

```shell
doggiesOffsetDumper [global options] command [command options] [arguments...]

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --config value  The config file to use (default: "config.hcl")
   --file value    The binary file to use
   --help, -h      show help (default: false)
```

## Example Output

```shell
                 state(0x2C9A1) - pcdogs.exe + 0x9FA80
             level_end(0x2E1BF) - pcdogs.exe + 0x168D360
              level_id(0x2E978) - pcdogs.exe + 0x168D9B0
              level_ptr(0x4091) - pcdogs.exe + 0x168D9A0
 level_var_list_offset(0x3D99C) - 0x1C
           first_actor(0x13106) - pcdogs.exe + 0x23765C

```