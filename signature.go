package main

import (
	"bufio"
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

type SignatureConfig struct {
	Label   string `hcl:"label,label"`
	Pattern string `hcl:"pattern"`
	Offset  int    `hcl:"offset"`
	Size    int    `hcl:"size"`
	Type    string `hcl:"type"`
}

type SignatureResult struct {
	Value  int
	Offset int
}

func (c SignatureConfig) FormatResult(r *SignatureResult, binaryPath string) string {
	binaryName := filepath.Base(binaryPath)

	labelFormat := fmt.Sprintf("%s(0x%X)", c.Label, r.Offset)
	valueFormat := fmt.Sprintf("0x%X", r.Value)

	if c.Type == "ptr" {
		valueFormat = fmt.Sprintf("%s + 0x%X", binaryName, r.Value-0x400000)
	}

	return fmt.Sprintf("%25s - %s", labelFormat, valueFormat)
}

func (c SignatureConfig) Resolve(binaryPath string) (*SignatureResult, error) {
	f, err := os.Open(binaryPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	offset, err := findSignature(f, c.Pattern)
	if err != nil {
		return nil, err
	}

	offset += c.Offset

	value, err := readBigEndian(f, offset, c.Size)
	if err != nil {
		return nil, err
	}

	return &SignatureResult{
		Value:  value,
		Offset: offset,
	}, nil
}

func findSignature(f *os.File, sig string) (int, error) {
	sigData := splitSig(sig)

	r := bufio.NewReader(f)

	currentSigByte := 0
	currentOffset := 0

	for b, err := r.ReadByte(); err != io.EOF; b, err = r.ReadByte() {
		if err != nil {
			return 0, err
		}
		currentOffset++

		if sigData[currentSigByte] == "?" {
			currentSigByte++
			continue
		}

		sigBytes, err := hex.DecodeString(sigData[currentSigByte])
		if err != nil {
			return 0, err
		}

		sigByte := sigBytes[0]

		if b != sigByte {
			currentSigByte = 0
			continue
		}

		currentSigByte++
		if currentSigByte >= len(sigData) {
			return currentOffset - len(sigData), nil
		}
	}

	return 0, fmt.Errorf("failed to find sig in file")
}

func splitSig(sig string) []string {
	return strings.Split(strings.TrimPrefix(sig, " "), " ")
}
